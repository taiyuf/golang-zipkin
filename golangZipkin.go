package golangzipkin

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	zipkin "github.com/openzipkin/zipkin-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/middleware/http"
	"github.com/openzipkin/zipkin-go/model"
	reporterhttp "github.com/openzipkin/zipkin-go/reporter/http"
)

// Tracer is the global instance of zipkin.Tracer.
// var Tracer *zipkin.Tracer

type Zipkin struct {
	Tracer     *zipkin.Tracer
	HTTPClient *HTTPClient
}

// newTracer is the private method return the tracer instance. please use GetTracer method!
func newTracer(serviceName, host string, port int, endPointURL string, rate float64) (*zipkin.Tracer, error) {
	// The reporter sends traces to zipkin server
	reporter := reporterhttp.NewReporter(endPointURL)

	// create our local service endpoint
	endpoint, eerr := zipkin.NewEndpoint(serviceName, host+":"+strconv.Itoa(port))
	if eerr != nil {
		return nil, fmt.Errorf(`unable to create local endpoint: %+v\n`, eerr)
	}

	// Sampler tells you which traces are going to be sampled or not. In this case we will record 100% (1.00) of traces.
	var r float64
	if rate == 0 {
		r = 1
	}

	sampler, cerr := zipkin.NewCountingSampler(r)
	if cerr != nil {
		return nil, cerr
	}

	// initialize our tracer
	tracer, terr := zipkin.NewTracer(
		reporter,
		zipkin.WithSampler(sampler),
		zipkin.WithLocalEndpoint(endpoint),
	)
	if terr != nil {
		return nil, fmt.Errorf(`unable to create tracer: %+v\n`, terr)
	}

	return tracer, nil
}

// GetTracer return the tracer instance.
func NewZipkin(host string, port int, url, name string, rate float64) (*Zipkin, error) {
	ePrefix := "zipkin::GetTracer:"

	if err := checkZipkin(host, port, name, url); err != nil {
		return &Zipkin{}, fmt.Errorf("%v %v", ePrefix, err)
	}

	// if Tracer == nil {
	// 	var err error

	tracer, err := newTracer(
		name,
		host,
		port,
		url,
		rate,
	)
	if err != nil {
		return &Zipkin{}, fmt.Errorf("%v %v", ePrefix, err)
	}
	// }

	client, herr := NewHTTPClient(tracer)
	if herr != nil {
		return &Zipkin{}, fmt.Errorf("%v %v", ePrefix, herr)
	}

	return &Zipkin{
		Tracer:     tracer,
		HTTPClient: client,
	}, nil
}

// HTTPClient

// HTTPClient is struct of client of http or zipkinhttp. if zipkin is enable, work as the zipkinhttp client, or do as ordinary http.Client.
type HTTPClient struct {
	HTTPClient   *http.Client
	ZipkinClient *zipkinhttp.Client
}

// Do execute to request with the zipkin tracer, if there is no tracer, request ordinary.
func (c *HTTPClient) Do(req *http.Request) (*http.Response, error) {
	if c.ZipkinClient == nil {
		return c.HTTPClient.Do(req)
	}

	routePath, rerr := GetSpanName(req)
	if rerr != nil {
		return nil, rerr
	}

	return c.ZipkinClient.DoWithAppSpan(req, routePath)
}

// NewHTTPClient return the HTTPClient instance implement the Do method.
func NewHTTPClient(t *zipkin.Tracer) (*HTTPClient, error) {
	ePrefix := "*** zipkin::NewHTTPClient: "
	c := &HTTPClient{}

	c.HTTPClient = &http.Client{}

	zipkinCLient, cerr := zipkinhttp.NewClient(t, zipkinhttp.ClientTrace(true))
	if cerr != nil {
		return c, fmt.Errorf("%v %v", ePrefix, cerr)
	}

	c.ZipkinClient = zipkinCLient

	return c, nil
}

// span

// span sturuct is the alternative span object.
// if zipkin is enable, work as zipkin's span. or do nothing.
type Span struct {
	Span *zipkin.Span
}

// Tag set the key and value to span. if zipkin is disable, do nothing.
func (s *Span) Tag(key, value string) {
	if s.Span != nil {
		(*s.Span).Tag(key, value)
	}
}

// Annotate set the period with the timestamp to span. if zipkin is disable, do nothing.
func (s *Span) Annotate(time time.Time, str string) {
	if s.Span != nil {
		(*s.Span).Annotate(time, str)
	}
}

// Context return the span context.  if zipkin is disable, return the empty model.SpanContext.
func (s *Span) Context() model.SpanContext {
	if s.Span != nil {
		return (*s.Span).Context()
	}

	return model.SpanContext{}
}

// SetRemoteEndpoint set the end point of the zipkin server. if zipkin is disable, do nothing.
func (s *Span) SetRemoteEndpoint(endpoint *model.Endpoint) {
	if s.Span != nil {
		(*s.Span).SetRemoteEndpoint(endpoint)
	}
}

// SetName set the name of span. if zipkin is disable, do nothing.
func (s *Span) SetName(name string) {
	if s.Span != nil {
		(*s.Span).SetName(name)
	}
}

// Finish finish the span. if zipkin is disable, do nothing.
func (s *Span) Finish() {
	if s.Span != nil {
		(*s.Span).Finish()
	}
}

// Flush flush the span. if zipkin is disable, do nothing.
func (s *Span) Flush() {
	if s.Span != nil {
		(*s.Span).Flush()
	}
}

// public methods

// ZipkinInitialize initialize the zipkin instance. must be executed first!
func ZipkinInitialize(host string, port int, url, name string, rate float64) (*Zipkin, error) {
	z, err := NewZipkin(host, port, url, name, rate)
	if err != nil {
		return &Zipkin{}, err
	}

	return z, nil
}

// CheckZipkin validate the configuration of zipkin.
func checkZipkin(host string, port int, name, url string) error {
	prefix := ""

	if host == "" {
		return fmt.Errorf("%v%v", prefix, "No host name.")
	}

	if port == 0 {
		return fmt.Errorf("%v%v", prefix, "No port number.")
	}

	if name == "" {
		return fmt.Errorf("%v%v", prefix, "No service name.")
	}

	if url == "" {
		return fmt.Errorf("%v%v", prefix, "No URL.")
	}

	return nil
}

// UseMiddlewareZipkin use the middleware for zipkin and return http.Handler.
func (z *Zipkin) UseMiddlewareZipkin(serviceName string, r *mux.Router) http.Handler {

	if z.Tracer == nil {
		return r
	}

	r.Use(zipkinhttp.NewServerMiddleware(
		z.Tracer,
		zipkinhttp.TagResponseSize(true),
		zipkinhttp.SpanName(serviceName),
	))

	return r
}

// GetSpanName return the name of span and error.
func GetSpanName(req *http.Request) (string, error) {
	pathInfo := fmt.Sprintf("%v %v://%v%v", req.Method, req.URL.Scheme, req.URL.Host, req.URL.Path)

	route := mux.CurrentRoute(req)
	if route == nil {
		return pathInfo, nil
	}

	routePath, rerr := route.GetPathTemplate()
	if rerr != nil {
		return pathInfo, nil
	}

	return fmt.Sprintf("%v %v://%v%v", req.Method, req.URL.Scheme, req.URL.Host, routePath), nil
}

// GetSpan return the span instance with the request and context.
func (z *Zipkin) GetSpan(req *http.Request) (*Span, context.Context) {
	ctx := req.Context()

	if z.Tracer == nil {
		return &Span{}, ctx
	}

	span := zipkin.SpanFromContext(ctx)
	if span == nil {
		spanName, _ := GetSpanName(req)
		span, ctx = z.Tracer.StartSpanFromContext(context.Background(), spanName)
	}

	return &Span{
		Span: &span,
	}, ctx
}

// GetNewContext return the context with zipkin, if zipkin is disabled, return new context.
func GetNewContext(req *http.Request, span zipkin.Span) context.Context {
	ctx := req.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	return zipkin.NewContext(ctx, span)
}
