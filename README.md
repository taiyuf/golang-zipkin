# golang_zipkin

## サーバー側の事前処理

一番最初に、ZipkinInitializeを実行してください。

```
    z, err := ZipkinInitialize(
		ZipkinHost,
		ZipkinPort,
		ZipkinURL,
		ZipkinServiceName,
		ZipkinRate,
	)
	if err != nil {
		l.Info(fmt.Sprintf("*** zipkin::ZipkinInitialize: zipkin has been disabled: %v", err))
	}
```

次に、サーバー側のハンドラーの部分で、UseMiddlewareZipkinを使ってください。

```
	r := mux.NewRouter()

	// some
	r.HandleFunc(UpdateRemoteIP, GreatSomeHandlerFunc).Methods("GET")

	return z.UseMiddlewareZipkin("GreatServiceName", r) // => http.Handler

```

## パフォーマンス測定

### Annotateで処理を分割する

```
	// spanを作成
	span, _ := z.GetSpan(r)

	// Annotateで、処理を分割する
	// spanのタイムライン上で、Annotateしたところにタイムスタンプが打たれ、処理にかかった時間が計算される
	span.Annotate(time.Now(), "start some process")

	// Tagはあくまでも、この処理の中での変数や何かのメッセージを残しておきたいものを記述するのに使う
	span.Tag("key", "value")
```

### HTTPのRESTの処理の詳細な測定

* 専用のHTTPClientを生成する

```
	client, _ := z.HTTPClient
```

* 通常のリクエスト処理をする前に、contextとspanを使って、リクエストを作り直し、実行する

```
	newReq := req.WithContext(helper.GetNewContext(req, *span))
	res, err := client.Do(newReq)
	if err != nil {
		return "", fmt.Errorf("%v", err)
	}
	defer res.Body.Close()
```
